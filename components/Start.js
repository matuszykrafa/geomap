import React, { Component } from 'react';
import { View, Text } from 'react-native';
import * as Font from 'expo-font'

import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Start extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      fontloaded: false,
    };
  }
  componentWillMount = async () => {
    await Font.loadAsync({
      'myfont': require('../assets/font2.ttf'),
    });
    this.setState({ fontloaded: true })
  }
  render() {
    return (
      this.state.fontloaded
        ?
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3f51b5' }}>
            <Text style={{ fontSize: 100, color: 'white', fontFamily: 'myfont', textAlign: "center" }}>GeoMap App</Text>
            <Text style={{ fontSize: 60, color: 'white', fontFamily: 'myfont', textAlign: "center" }}>find and save your location</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => {
              this.props.navigation.navigate('main')
            }}>
              <Text style={{ fontWeight: 'bold' }}>START</Text>
            </TouchableOpacity>
          </View>
        </View>
        :
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#3f51b5' }}>
            <Text style={{ fontSize: 80, color: 'white', textAlign: "center" }}>GeoMap App</Text>
            <Text style={{ fontSize: 40, color: 'white', textAlign: "center" }}>find and save your location</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => {
              this.props.navigation.navigate('main')
            }}>
              <Text style={{ fontWeight: 'bold' }}>START</Text>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}
