import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage, Switch } from 'react-native';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'

import Item from './Item'
import MyButton from './MyButton'
import { switchCase } from '@babel/types';

export default class Main extends Component {
    static navigationOptions = {
        title: 'Zapis pozycji',
        headerStyle: {
            backgroundColor: '#3f51b5'
        },
        headerTitleStyle: {
            color: 'white'
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            locations: [],
            switches: [],
            selectAll: false
        };
    }

    async componentDidMount() {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            alert('brak dostepu do lokalizacji')
        }

        let keys = await AsyncStorage.getAllKeys()
        let stores = await AsyncStorage.multiGet(keys)

        // console.log(stores)
        let values = stores.map((result, i, store) => { return JSON.parse(store[i][1]) });
        this.setState({ loading: false, locations: values, switches: values.map(x => false), selectAll: false })

    }

    render() {
        let i = 0
        let data = this.state.locations.map(item => ({ index: i++, pos: item }))
        if (this.state.loading)
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../assets/loading.gif')} />
                </View>
            );
        else
            return (
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                            <MyButton text='POBIERZ I ZAPISZ POZYCJĘ' onPress={async () => {
                                this.setState({ loading: true })
                                let pos = await Location.getCurrentPositionAsync({})
                                await AsyncStorage.setItem(pos.timestamp.toString(), JSON.stringify(pos))
                                let locations = this.state.locations
                                locations.push(pos)
                                let switches = this.state.switches
                                switches.push(false)
                                this.setState({ loading: false, locations: locations, switches: switches })
                            }} />
                            <MyButton text='USUŃ WSZYSTKIE DANE' onPress={async () => {
                                this.setState({ loading: true })
                                await AsyncStorage.clear()
                                this.setState({ loading: false, locations: [], switches: [] })
                            }} />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ flex: 9 }}>
                                <MyButton text='PRZEJDŹ DO MAPY' onPress={() => {
                                    let i = 0
                                    let locations = this.state.locations.filter(x => this.state.switches[i++])
                                    if (locations.length > 0)
                                        this.props.navigation.navigate('map', { locations: locations })
                                    else
                                        alert('Musisz zaznaczyć co najmniej jedną lokalizację')
                                }} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <Switch value={this.state.selectAll} onValueChange={function () {
                                    let enabled = this.state.selectAll
                                    let switches = this.state.switches
                                    if (!enabled)
                                        switches = switches.map(x => true)
                                    else
                                        switches = switches.map(x => false)
                                    this.setState({ selectAll: !enabled, switches: switches })
                                }.bind(this)} />
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 9 }}>
                        <FlatList data={data}
                            renderItem={({ item }) => <Item pos={item.pos} key={item.index} onValueChange={function () {
                                let switches = this.state.switches
                                switches[item.index] = !switches[item.index]
                                this.setState({ switches: switches })
                            }.bind(this)} switch={this.state.switches[item.index]} />}
                            keyExtractor={(item, index) => index.toString()} />
                    </View>
                </View>
            );
    }
}
