import React, { Component } from 'react';
import { View, Text } from 'react-native';
import MapView from 'react-native-maps'

export default class Map extends Component {
    static navigationOptions = {
        title: 'mapa',
        headerStyle: {
            backgroundColor: '#3f51b5'
        },
        headerTitleStyle: {
            color: 'white'
        }
    }
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        const locations = this.props.navigation.state.params.locations
        const latitudes = locations.map(x => x.coords.latitude)
        const longitudes = locations.map(x => x.coords.longitude)
        const avg_latitude = latitudes.reduce((a, b) => a + b, 0) / latitudes.length
        const avg_longitude = longitudes.reduce((a, b) => a + b, 0) / longitudes.length
        let i = 0

        var tab = locations.map(x => <MapView.Marker coordinate={{ latitude: x.coords.latitude, longitude: x.coords.longitude }} title={"ugh"} description={"ughh"} key={i++} />)

        return (
            <MapView style={{ flex: 1 }} initialRegion={{ latitude: avg_latitude, longitude: avg_longitude, latitudeDelta: 0.001, longitudeDelta: 0.001 }}>
                {tab}
            </MapView>
        );
    }
}
