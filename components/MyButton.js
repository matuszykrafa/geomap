import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class MyButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ margin: 10 }}>
        <TouchableOpacity onPress={this.props.onPress}>
          <Text style={{ fontWeight: 'bold' }}>{this.props.text}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
