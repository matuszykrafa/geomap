import React, { Component } from 'react';
import { View, Text, Image, Switch } from 'react-native';

export default class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={require('../assets/bee.png')} style={{ width: 50, height: 50 }} />
        </View>
        <View style={{ flex: 2 }}>
          <Text>timestamp: {this.props.pos.timestamp}</Text>
          <Text style={{ fontSize: 10, color: 'grey' }}>latitude: {this.props.pos.coords.latitude}</Text>
          <Text style={{ fontSize: 10, color: 'grey' }}>longitude: {this.props.pos.coords.longitude}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Switch onValueChange={this.props.onValueChange} value={this.props.switch} />
        </View>
      </View>
    );
  }
}
