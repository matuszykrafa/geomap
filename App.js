import {createStackNavigator, createAppContainer } from "react-navigation";
import Start from './components/Start'
import Main from './components/Main'
import Map from './components/Map'

const Root = createStackNavigator({
  start: { screen: Start },
  main: { screen: Main },
  map: { screen: Map }
});

const App = createAppContainer(Root);

export default App;